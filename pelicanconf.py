#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

def lookup_lang_name(lang_code):
    if lang_code in I18N_SUBSITES:
        return I18N_SUBSITES[lang_code]['NAME']
    elif lang_code == 'fr':
        return 'version française'
    else:
        return ''

JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
JINJA_FILTERS = {
    'lookup_lang_name': lookup_lang_name,
}

PLUGIN_PATHS = ['plugins']
PLUGINS  = ['i18n_subsites']
I18N_SUBSITES = {
    'en': {
        'SITENAME': 'hypoglycemia',
        'TIMELINEJS_TITLE': 'scheduling',
        'TIMELINEJS_SUBTITLE': 'shows and events',
        'TIMELINEJS_LANG': 'en',
        'NAME': 'english version'
    }
}

AUTHOR = 'Grégory DAVID'
SITENAME = 'hypoglycémie'
SITEURL = ''

THEME = "themes/hypoglycemie"

PATH = 'content'
USE_FOLDER_AS_CATEGORY = True
DISPLAY_CATEGORIES_ON_MENU = False

TIMEZONE = 'Europe/Paris'
DEFAULT_DATE = 'fs'
DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

MUSICBRAINZ_FQDN = 'test.musicbrainz.org'
MUSICBRAINZ_ARTIST_ID = '4ba4bc46-4cee-46c7-a655-8270a9f20575'
TIMELINEJS_TITLE = 'agenda'
TIMELINEJS_SUBTITLE = 'des concerts hypnopompiques'
TIMELINEJS_LANG = DEFAULT_LANG

AUTHOR_URL = 'diabetique/{slug}.html'
AUTHOR_SAVE_AS = 'diabetique/{slug}.html'
